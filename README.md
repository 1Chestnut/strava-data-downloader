# strava-data-downloader

## Config file
1. Create a `config.ini` file in `.app-data/.config/`
2. Fill in the `client_id` and `client_secret` details. They can be found here: https://www.strava.com/settings/api
3. Not it is turn to find your `code`. For that copy the below url into your browser, but replace `XXXXX` by your `client_id` 
   1. `http://www.strava.com/oauth/authorize?client_id=XXXXX&response_type=code&redirect_uri=http://localhost/exchange_token&approval_prompt=force&scope=profile:read_all,activity:read_all`
4. It needs authorisation to access your information. Please continue as appropriate for you.
5. Once authorised the browser will open a new screen. Go to the new generated url and copy the string in the code field (marked as XXX...XX below).
   1. `http://localhost/exchange_token?state=&code=XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX&scope=read,activity:read_all,profile:read_all`

More information on this process can be found here: 
https://medium.com/swlh/using-python-to-connect-to-stravas-api-and-analyse-your-activities-dummies-guide-5f49727aac86

## Usage
- Use command `python download_data.py dwtype` in terminal
    - where `dwtype` can be `activities`
