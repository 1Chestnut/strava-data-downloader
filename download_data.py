# -*- coding: utf-8 -*-

'''
Description:
- Download data from Strava

Requirements:
- config.ini should be complete. See README for more info.

Usage:
- usage: python download_data.py activities
'''

import os
import configparser
import pandas as pd
import requests
import json
import time
import argparse

# Arguments parser
parser = argparse.ArgumentParser()
parser.add_argument("dw_type")
args = parser.parse_args()

# Reading config file
config = configparser.ConfigParser()
config.read('.app-data/.config/config.ini')
config_client_id = config['StravaAuth']['client_id']
config_client_secret = config['StravaAuth']['client_secret']
config_code = config['StravaAuth']['code']

# Get token for first time connection. client_code, client_secret and code are needed
if (os.path.exists(".app-data/.tokens/strava_tokens.json") == False):
    response = requests.post(
                        url = 'https://www.strava.com/oauth/token',
                        data = {
                                'client_id': config_client_id,
                                'client_secret': config_client_secret,
                                'code': config_code,
                                'grant_type': 'authorization_code'
                                }
                    )
    # Save json response as a variable
    strava_tokens = response.json()
    # Save tokens to file
    with open('.app-data/.tokens/strava_tokens.json', 'w') as outfile:
        json.dump(strava_tokens, outfile)
    # Open JSON file and print the file contents to check it's worked properly
    with open('.app-data/.tokens/strava_tokens.json') as check:
      data = json.load(check)
    print(data)
else:
    # Get the tokens from file to connect to Strava
    with open('.app-data/.tokens/strava_tokens.json') as json_file:
        strava_tokens = json.load(json_file)

# If access_token has expired then use the refresh_token to get the new access_token
if strava_tokens['expires_at'] < time.time():

    # Make Strava auth API call with current refresh token
    response = requests.post(
                        url = 'https://www.strava.com/oauth/token',
                        data = {
                                'client_id': config_client_id,
                                'client_secret': config_client_secret,
                                'grant_type': 'refresh_token',
                                'refresh_token': strava_tokens['refresh_token']
                                }
                    )
    # Save response as json in new variable
    new_strava_tokens = response.json()
    
    # Save new tokens to file
    with open('.app-data/.tokens/strava_tokens.json', 'w') as outfile:
        json.dump(new_strava_tokens, outfile)
    
    #Use new Strava tokens from now
    strava_tokens = new_strava_tokens

# Loop through all activities
if (args.dw_type == "activities"):
    page = 1
    url = "https://www.strava.com/api/v3/activities"
    access_token = strava_tokens['access_token']

    print("downloading activities")
    ## Create the dataframe ready for the API call to store your activity data
    activities = pd.DataFrame(
        columns = [
                "id",
                "name",
                "start_date_local",
                "type",
                "distance",
                "moving_time",
                "elapsed_time",
                'average_speed',
                'max_speed',
                'average_heartrate',
                'max_heartrate',
                'elev_high',
                'elev_low',
                "total_elevation_gain",
                'kudos_count',
                "end_latlng",
                "external_id"
        ]
    )
    while True:
        
        # get page of activities from Strava
        r = requests.get(url + '?access_token=' + access_token + '&per_page=200' + '&page=' + str(page))
        r = r.json()
        
        # if no results then exit loop
        if (not r):
            break

        # otherwise add new data to dataframe
        for x in range(len(r)):
            activities.loc[x + (page-1)*200,'id'] = r[x]['id']
            activities.loc[x + (page-1)*200,'name'] = r[x]['name']
            activities.loc[x + (page-1)*200,'start_date_local'] = r[x]['start_date_local']
            activities.loc[x + (page-1)*200,'type'] = r[x]['type']
            activities.loc[x + (page-1)*200,'distance'] = r[x]['distance']
            activities.loc[x + (page-1)*200,'moving_time'] = r[x]['moving_time']
            activities.loc[x + (page-1)*200,'elapsed_time'] = r[x]['elapsed_time']
            activities.loc[x + (page-1)*200,'average_speed'] = r[x]['average_speed']
            activities.loc[x + (page-1)*200,'max_speed'] = r[x]['max_speed']
            activities.loc[x + (page-1)*200,'average_heartrate'] = r[x]['average_heartrate'] if 'average_heartrate' in r[x] else 0
            activities.loc[x + (page-1)*200,'max_heartrate'] = r[x]['max_heartrate'] if 'max_heartrate' in r[x] else 0
            activities.loc[x + (page-1)*200,'elev_high'] = r[x]['elev_high']if 'elev_high' in r[x] else 0
            activities.loc[x + (page-1)*200,'elev_low'] = r[x]['elev_low']if 'elev_low' in r[x] else 0
            activities.loc[x + (page-1)*200,'total_elevation_gain'] = r[x]['total_elevation_gain']
            activities.loc[x + (page-1)*200,'kudos_count'] = r[x]['kudos_count']
            activities.loc[x + (page-1)*200,'end_latlng'] = r[x]['end_latlng']
            activities.loc[x + (page-1)*200,'external_id'] = r[x]['external_id']

        # increment page
        page += 1
    activities.to_csv('.app-data/.data/strava_activities_v2.csv', index=False)
